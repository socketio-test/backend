const bodyParser = require('body-parser');
const express = require('express');

class RestService {
  constructor (app, tokenService) {
    this._app = app;
    this._tokenService = tokenService;
  }

  async start () {
    const app = this._app;

    app.use(bodyParser.json());

    app.use('/', express.static(__dirname + '/public'));
    app.all('/ping', this._ping);
    app.post('/join', this._postJoin);

    app.use(this._notFoundHandler);
    app.use(this._errorHandler);
  }

  _ping = (req, res, next) => {
    res.json({ result: 'Pong' });
  };

  _postJoin = (req, res, next) => {
    const { identity } = req.body;

    if (!identity) {
      return res.status(400).json({
        error: 'identity is required'
      });
    }

    this._tokenService.getToken(identity)
      .then(token => res.json({ result: { token } }))
      .catch(next);
  };

  _notFoundHandler = (req, res, next) => {
    res.status(404).json({ result: 'Not Found' });
  };

  _errorHandler = (err, req, res, next) => {
    let message = 'Internal Server Error';

    if (this._app.get('env') === 'development') {
      err || (err = { message: 'Unknown Error' });
      message = err.message;
    }

    res.status(500).json({ error: message });
  };
}

module.exports = RestService;
