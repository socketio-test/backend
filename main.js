const http = require('http');
const express = require('express');
const socketIo = require('socket.io');

const TokenService = require('./token-service');
const RestService = require('./rest-service');
const SocketService = require('./socket-service');

const HANDLE = process.argv[2] || process.env.PORT || 8080;

async function main () {
  const app = express();
  const server = new http.Server(app);
  const io = socketIo(server);

  const tokenService = new TokenService();
  const restService = new RestService(app, tokenService);
  const socketService = new SocketService(io, tokenService);

  await restService.start();
  await socketService.start();

  server.listen(HANDLE);

  return new Promise((resolve, reject) => {
    server.on('listening', resolve);
    server.on('error', reject);
  });
}

main()
  .then(() => console.log(`Server started at ${HANDLE}`))
  .catch(console.error);
