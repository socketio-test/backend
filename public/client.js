jQuery(function ($, undefined) {
  'use strict';

  function join (identity) {
    return $.post({
      url: '/join',
      data: JSON.stringify({ identity: identity }),
      dataType: 'json',
      contentType: 'application/json'
    });
  }

  function $sent (message) {
    return $('<li></li>').addClass('bubble sent')
      .append($('<div></div>').html(message));
  }

  function $received (from, message) {
    return $('<li></li>').addClass('bubble received')
      .append(
        $('<div></div>')
          .append($('<i></i>').html(from))
          .append($('<span></span>').html(message))
      );
  }

  function $error (error) {
    return $('<li></li>').addClass('error').html(error);
  }

  var app = {};

  app.main = function () {
    app.$blocker = $('#blocker');
    app.$conversation = $('#conversation');
    app.$footer = $('#footer');
    app.$message = app.$footer.find('.message');
    app.$sendButton = app.$footer.find('.send-button');

    app.$footer.submit(app.onSend);

    setTimeout(function () {
      app.promptIdentity();
    });
  };

  app.uiBlock = function (block) {
    app.$blocker.toggleClass('hidden', !block);
  };

  app.uiBlockFooter = function (block) {
    app.$message.attr('disabled', block);
    app.$sendButton.attr('disabled', block);
  };

  app.uiMessageText = function () {
    return app.$message.val();
  };

  app.uiClearMessage = function () {
    app.$message.val('');
  };

  app.uiBubble = function ($bubble) {
    app.$conversation.append($bubble);
  };

  app.uiFocusMessage = function () {
    app.$message.focus();
  };

  app.uiAutoScroll = function () {
    app.$conversation.scrollTop(app.$conversation.prop('scrollHeight'));
  };

  app.onSend = function (e) {
    e.preventDefault();

    var message = app.uiMessageText();
    app.uiBlockFooter(true);

    app.socket.emit('send', { message: message }, function () {
      app.uiBlockFooter(false);
      app.uiClearMessage();
      app.uiFocusMessage();
      app.uiBubble($sent(message));
      app.uiAutoScroll();
    });
  };

  app.promptIdentity = function () {
    app.identity = null;
    while (!app.identity) {
      app.identity = prompt('Please enter your name (e.g. John Due):');
    }

    join(app.identity)
      .then(app.joined)
      .catch(function (err) {
        app.ajaxError(err);
        app.promptIdentity();
      });
  };

  app.joined = function (data) {
    if (!data || !data.result || !data.result.token) {
      return app.promptIdentity();
    }

    app.token = data.result.token;
    app.connectSocket();
  };

  app.ajaxError = function (err) {
    var title = 'Error';
    var desc = 'Unknown error. Please check your internet connection.';

    if (err.statusText) {
      title = err.statusText;
    }

    if (err.responseJSON && err.responseJSON.error) {
      desc = err.responseJSON.error;
    }

    app.uiBubble($error(title + ': ' + desc));
  };

  app.socketError = function (err) {
    if (!err || typeof err !== 'string') {
      err = 'Unknown Socket Error';
    }

    app.uiBubble($error(err));
  };

  app.connectSocket = function () {
    app.socket = io.connect('/', {
      forceNew: true,
      query: { token: app.token }
    });

    app.socket.on('connect', app.socketConnected);
    app.socket.on('error', app.socketError);
    app.socket.on('message', app.messageReceived);
  };

  app.socketConnected = function () {
    app.uiBlock(false);
  };

  app.messageReceived = function (data) {
    if (!data || !data.message) {
      return;
    }

    app.uiBubble($received(data.identity || 'Unknown', data.message));
    app.uiAutoScroll();
  };

  app.main();
});
