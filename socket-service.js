class SocketService {
  constructor (io, tokenService) {
    this._io = io;
    this._tokenService = tokenService;
  }

  async start () {
    this._io
      .of('/')
      .use(this._checkToken)
      .on('connect', this._onConnect);
  }

  _onConnect = (socket) => {
    socket.on('send', (data, ack) => this._onSend(socket, data, ack));
  };

  _onSend = (socket, data, ack) => {
    const { message } = data;
    const { identity } = socket;

    if (typeof message !== 'string' || !message) {
      return ack({ error: 'Message must be non-empty string' });
    }

    socket.broadcast.emit('message', { identity, message });
    ack({ result: 'OK' });
  };

  _checkToken = (socket, next) => {
    const hs = socket.handshake;
    const token = hs.query ? hs.query.token : null;

    if (!token) {
      return next(new Error('Token query param is required'));
    }

    const identity = this._tokenService.getIdentity(token);

    this._tokenService.getIdentity(token)
      .then(identity => {
        if (!identity) {
          return next(new Error('Invalid token'));
        }

        socket.identity = identity;
        socket.token = token;

        next();
      });
  };
}

module.exports = SocketService;
