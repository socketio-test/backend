const uuid = require('uuid').v4;

class MockTokenService {
  _identities = new Map();
  _tokens = new Map();

  _add (identity) {
    const tkn = uuid();

    this._identities.set(tkn, identity);
    this._tokens.set(identity, tkn);

    return tkn;
  }

  async getToken (identity) {
    return this._tokens.get(identity) || this._add(identity);
  }

  async getIdentity (token) {
    return this._identities.get(token);
  }
}

module.exports = MockTokenService;
