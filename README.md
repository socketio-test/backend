## Socket.io Test
Minimal socket.io chat implemantation.

### How to Run?
Run `npm i && npm start` and open [:8080](http://localhost:8080 ":8080") in your preferred web browser.

---
[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png "WTFPL")](http://www.wtfpl.net/txt/copying/ "WTFPL")